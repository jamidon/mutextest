# MutexTest

----

Unpublished work © 2014, The Narwhal Group

All Rights Reserved

THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF THE NARWHAL GROUP.

The copyright notice above does not evidence any
actual or intended publication of such source code.

----

## Overview

Silly little VS2010 project for testing named [Mutex](http://msdn.microsoft.com/en-us/library/system.threading.mutex.aspx) objects that can be used across processes.
