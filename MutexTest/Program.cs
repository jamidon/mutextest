﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MutexTest
{
    class Program
    {
        static int Main(string[] args)
        {
            var sleepSeconds = 120;
            if (args.Length > 0)
            {
                int foo;
                if (int.TryParse(args[0], out foo))
                    sleepSeconds = foo;
            }
            int? mutexWait = null;
            if (args.Length > 1)
            {
                int foo;
                if (int.TryParse(args[1], out foo))
                    mutexWait = foo;
            }

            var m = new Mutex(false, @"Global\foobar");
            Console.WriteLine("Waiting for mutex {0}...", mutexWait.HasValue ? ("for upto " + mutexWait + " seconds") : "forever");
            var haveMutex = false;
            try
            {
                if (mutexWait.HasValue)
                    haveMutex = m.WaitOne(mutexWait.Value * 1000);
                else
                    haveMutex = m.WaitOne();
            }
            catch (AbandonedMutexException)
            { }

            if (!haveMutex)
                return 100;

            Console.WriteLine("Sleeping {0} seconds...", sleepSeconds);
            Thread.Sleep(sleepSeconds * 1000);
            m.ReleaseMutex();

            return 0;
        }
    }
}
